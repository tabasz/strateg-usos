function getSchedule(course)
// Funkcja przyjmuje kod przedmiotu i zwraca obiekt typu Przedmiot.
{
    console.log("getSchedule("+course+")");
    function httpGet(theUrl)
    {
        var xmlHttp = null;
        xmlHttp = new XMLHttpRequest();
        xmlHttp.open( "GET", theUrl, false );
        xmlHttp.send( null );
        return xmlHttp.responseText;
    }
    /*/
    Przedmiot = {String nazwa, Zajecia[] plan} - ten obiekt bedzie budowany z pojedynczych zajec lub wczytany ze storage
    Zajecia tZajecia; Zajecia = {Przedmiot, Typ, Godzina...}

    Otrzymane z API:
    String reqtxt
    oPlan = [z,z,z...] - zbior zajec w ramach przedmiotu
    z =
    /*/
    var tZajecia = {}; // temp, do dodawania do planu przedmiotu
    var Przedmiot = {"nazwa":"", "plan":[]}
    var stored = {};
    chrome.storage.sync.get(course, function(result) {stored = result;})

    // reqtxt - tresc zapytania do API
    var reqtxt = httpGet("http://usosapps.uw.edu.pl/services/tt/course_edition?course_id=" + course + "&term_id=2013L&fields=course_name");
    Przedmiot.nazwa = JSON.parse(reqtxt)[0]["course_name"]["pl"];
    // reqtxt - znowu tresc innego zapytania do API. Z tego biore wszystkie dane
    reqtxt = httpGet("http://usosapps.uw.edu.pl/services/tt/course_edition?course_id=" + course + "&term_id=2013L&start=2014-05-26&fields=start_time|end_time|classtype_name|group_number|room_number|type");
    var oPlan = JSON.parse(reqtxt);
    var tZajecia = {};
    for (var i=0; i<oPlan.length; i++)
    {
        var z = oPlan[i];

        tZajecia["Przedmiot"] = Przedmiot.nazwa;
        tZajecia["Typ"] = z["classtype_name"]["pl"];
        tZajecia["NrGrupy"] = z["group_number"];
        tZajecia["NrSali"] = z["room_number"];
        tZajecia["GodzinaOd"] = z["start_time"];
        tZajecia["GodzinaDo"] = z["end_time"];
        tZajecia["LiczbaKolizji"] = 0;
        tZajecia["CzyWybrany"] = false;

        Przedmiot.plan.push(JSON.parse( JSON.stringify( tZajecia )));
    }
    //alert(JSON.stringify(Przedmiot));
    return Przedmiot;
}
