$(document).ready(function() {
   var $calendar = $('#calendar');
   var id = 10;
	var naszjson = { events : []};
	var czekanie = true;
	var klik = true;
	var ost_wybrany = {};
	var czy_wybrany = true;
	var zmienione = true;
	var podwojny = false;
	
	function kolizje(calEvent)
	{
		//chrome.storage.sync.clear();
		chrome.storage.sync.get(null, function (v)
		{
			var tablica = {};
			var licznik = 0;
			var ilekluczy = Object.keys(v).length;
			for (key in v)
			{
				nazwa = v[key]["nazwa"];
				plan = v[key]["plan"];
    			for (i = 0; i < plan.length; i++) 
    			{
    				if (calEvent["title"] != (v[key]["nazwa"] + " " + plan[i]["Typ"] + " " + plan[i]["NrGrupy"]))
    				{
	    				if (((new Date (plan[i]["GodzinaOd"]) >= calEvent.start) &&
				 		(new Date (plan[i]["GodzinaOd"]) < calEvent.end)) ||
				 		((new Date (plan[i]["GodzinaDo"]) >= calEvent.start) &&
				 		(new Date (plan[i]["GodzinaDo"]) < calEvent.end)))
				 		{
				 			if (calEvent["CzyWybrany"] == false)
				 			{
				 				plan[i]["LiczbaKolizji"]--;
				 			}
				 			else
				 			{
				 				plan[i]["LiczbaKolizji"]++;
				 			}
				 		}
				 	}
				 	else
				 	{
				 		plan[i]["CzyWybrany"] = (plan[i]["CzyWybrany"]) ? false : true;
				 		if (new Date (plan[i]["GodzinaOd"]).getDate() != calEvent.start.getDate())
				 		{
				 			for (pom in v)
				 			{
				 				plan_pom = v[pom]["plan"];
				 				for (var j = 0; j < plan_pom.length; j++)
				 				{
				 					if (calEvent["title"] != (v[pom]["nazwa"] + " " + plan_pom[j]["Typ"] + " " + plan_pom[j]["NrGrupy"]))
    								{
    									if (((new Date (plan_pom[j]["GodzinaOd"]) >= new Date (plan[i]["GodzinaOd"])) &&
								 		(new Date (plan_pom[j]["GodzinaOd"]) < new Date (plan[i]["GodzinaDo"]))) ||
								 		((new Date (plan_pom[j]["GodzinaDo"]) >= new Date (plan[i]["GodzinaOd"])) &&
								 		(new Date (plan_pom[j]["GodzinaDo"]) < new Date (plan[i]["GodzinaDo"]))))
								 		{
								 			if (plan[i]["CzyWybrany"] == false)
								 			{
								 				plan_pom[j]["LiczbaKolizji"]--;
								 			}
								 			else
								 			{
								 				plan_pom[j]["LiczbaKolizji"]++;
								 			}
								 		}
    								}
				 				}
				 			}
    					}
    				}
    			}
    			tablica[key] = v[key];
				chrome.storage.sync.set(tablica, function()
    			{
    					licznik++;
						if (licznik == ilekluczy)
						{
							//alert("odsiewzam");
							$("#calendar").weekCalendar("refresh");
						}
    			});
			}
			//alert("odswiezam");
			//$("#calendar").weekCalendar("refresh");

		});
		//$("#calendar").weekCalendar("refresh");
	}
	
	function kolor(calEvent, $event)
	{
		//alert("kolor");
		//alert(calEvent["title"] + " " + calEvent["CzyWybrany"]);
		if (calEvent["CzyWybrany"] == true) {
            $event.css("backgroundColor","#70DB70");
            $event.find(".wc-time").css({
               "backgroundColor" : "#4C8033",
               "border" : "1px solid #888"
            });
         }
		else if (calEvent["LiczbaKolizji"] > 0)
         {
         	$event.css("backgroundColor", "#b20000");
            $event.find(".wc-time").css({
               "backgroundColor" : "#660000",
               "border" : "1px solid #888"
            });
         }
         else
         {
         	$event.css("backgroundColor", "#68a1e5");
            $event.find(".wc-time").css({
               "backgroundColor" : "#2b72d0",
               "border" : "1px solid #1b62c0"
            });
         }
	}

	
	function odswiez() {
		alert("odswiez");
		chrome.storage.sync.get(null, function (v) { 
		var j = 1;
		var tablica = [];
   		for (key in v) 
   		{
   			nazwa = v[key]["nazwa"];
    		plan = v[key]["plan"];
    		for (i = 0; i < plan.length; i++) {
	      		spotkanie = {}
	      		spotkanie["id"] = j;
	      		j++;
	      		spotkanie["title"] = nazwa + " " + plan[i]["Typ"] + " " + plan[i]["NrGrupy"];
	      		spotkanie["start"] = new Date(plan[i]["GodzinaOd"]);
	      		spotkanie["end"] = new Date(plan[i]["GodzinaDo"]);
	      		spotkanie["LiczbaKolizji"] = plan[i]["LiczbaKolizji"];
	        	spotkanie["CzyWybrany"] = plan[i]["CzyWybrany"];
	        	
	        	if (spotkanie["title"] == ost_wybrany["title"])
	        	{
	        		if (spotkanie["CzyWybrany"] == czy_wybrany)
	        		{
	        			zmienione = false;
	        		}
	        		else
	        		{
	        			zmienione = true;
	        		}
	        	}
	      	
	      		tablica.push(spotkanie);
    		}
		}
		naszjson["events"] = tablica;

		});
	}
   $calendar.weekCalendar({
      timeslotsPerHour : 4,
      allowCalEventOverlap : true,
      overlapEventsSeparate: true,
      firstDayOfWeek : 1,
      businessHours :{start: 8, end: 20, limitDisplay: true },
      daysToShow : 7,
      height : function($calendar) {
         return $(window).height() - $("h1").outerHeight() - 1;
      },
    
      
      eventRender : function(calEvent, $event) {
      	//alert(zmienione);
      	/*if (zmienione == false) 
      	{
      		if (calEvent["title"] == ost_wybrany["title"])// && (calEvent["id"] == ost_wybrany["id"]))
      		{
      		//alert("wchodze w warunek " + calEvent["title"] + " calevent " + calEvent["CzyWybrany"] + " czy_wybr " + czy_wybrany);
	      		if (calEvent["CzyWybrany"] == czy_wybrany)
	      		{
	      			//alert("takiesame");
	      			$("#calendar").weekCalendar("refresh");
	      			zmienione = true;
	      		}
      			
      		//alert("rozne");
      		}
      	}
      	else
      	{*/
      		//alert(calEvent["title"] + ost_wybrany["title"] + calEvent["start"] + ost_wybrany["start"]);
        	kolor(calEvent, $event);
        //}
      },
      draggable : function(calEvent, $event) {
         return calEvent.readOnly != true;
      },
      resizable : function(calEvent, $event) {
         return calEvent.readOnly != true;
      },
      eventNew : function(calEvent, $event) {
         var $dialogContent = $("#event_edit_container");
         resetForm($dialogContent);
         var startField = $dialogContent.find("select[name='start']").val(calEvent.start);
         var endField = $dialogContent.find("select[name='end']").val(calEvent.end);
         var titleField = $dialogContent.find("input[name='title']");
         var bodyField = $dialogContent.find("textarea[name='body']");
         calEvent["CzyWybrany"] = false;
         calEvent["LiczbaKolizji"] = 0;


         $dialogContent.dialog({
            modal: true,
            title: "New Calendar Event",
            close: function() {
               $dialogContent.dialog("destroy");
               $dialogContent.hide();
               $('#calendar').weekCalendar("removeUnsavedEvents");
            },
            buttons: {
               save : function() {
                  calEvent.id = id;
                  id++;
                  calEvent.start = new Date(startField.val());
                  calEvent.end = new Date(endField.val());
                  calEvent.title = titleField.val();
                  calEvent.body = bodyField.val();

                  $calendar.weekCalendar("removeUnsavedEvents");
                  $calendar.weekCalendar("updateEvent", calEvent);
                  $dialogContent.dialog("close");
               },
               cancel : function() {
                  $dialogContent.dialog("close");
               }
            }
         }).show();

         $dialogContent.find(".date_holder").text($calendar.weekCalendar("formatDate", calEvent.start));
         setupStartAndEndTimeFields(startField, endField, calEvent, $calendar.weekCalendar("getTimeslotTimes", calEvent.start));

      },
      eventDrop : function(calEvent, $event) {
        
      },
      eventResize : function(calEvent, $event) {
      },
      eventClick : function(calEvent, $event) {
      	//alert("klik");
      	ost_wybrany = calEvent;
      	czy_wybrany = calEvent["CzyWybrany"];
      	zmienione = false;
      	//alert(calEvent["start"]);
      	calEvent["CzyWybrany"] = (calEvent["CzyWybrany"]) ? false : true;
      	//$("#calendar").weekCalendar("refresh");
      	//kolor(calEvent, $event);
      	kolizje(calEvent);
      	//$("#calendar").weekCalendar("refresh");

        /*if (calEvent.readOnly) {
            return;
         }

         var $dialogContent = $("#event_edit_container");
         resetForm($dialogContent);
         var startField = $dialogContent.find("select[name='start']").val(calEvent.start);
         var endField = $dialogContent.find("select[name='end']").val(calEvent.end);
         var titleField = $dialogContent.find("input[name='title']").val(calEvent.title);
         var bodyField = $dialogContent.find("textarea[name='body']");
         bodyField.val(calEvent.body);

         $dialogContent.dialog({
            modal: true,
            title: "Edit - " + calEvent.title,
            close: function() {
               $dialogContent.dialog("destroy");
               $dialogContent.hide();
               $('#calendar').weekCalendar("removeUnsavedEvents");
            },
            buttons: {
               save : function() {

                  calEvent.start = new Date(startField.val());
                  calEvent.end = new Date(endField.val());
                  calEvent.title = titleField.val();
                  calEvent.body = bodyField.val();

                  $calendar.weekCalendar("updateEvent", calEvent);
                  $dialogContent.dialog("close");
               },
               "delete" : function() {
                  $calendar.weekCalendar("removeEvent", calEvent.id);
                  $dialogContent.dialog("close");
               },
               cancel : function() {
                  $dialogContent.dialog("close");
               }
            }
         }).show();

         var startField = $dialogContent.find("select[name='start']").val(calEvent.start);
         var endField = $dialogContent.find("select[name='end']").val(calEvent.end);
         $dialogContent.find(".date_holder").text($calendar.weekCalendar("formatDate", calEvent.start));
         setupStartAndEndTimeFields(startField, endField, calEvent, $calendar.weekCalendar("getTimeslotTimes", calEvent.start));
         $(window).resize().resize(); //fixes a bug in modal overlay size ??
*/
      },
      eventMouseover : function(calEvent, $event) {
      },
      eventMouseout : function(calEvent, $event) {
      },
      noEvents : function() {
      },
      data : function(start, end, callback) {
         callback(getEventData());
      }
   });

   function resetForm($dialogContent) {
      $dialogContent.find("input").val("");
      $dialogContent.find("textarea").val("");
   }
   
   function getEventData() {
   	if (klik) {
   		odswiez();
   		if (zmienione == false)
		{
			$("#calendar").weekCalendar("refresh");
		}
		return naszjson;
		}
      var year = new Date().getFullYear();
      var month = new Date().getMonth();
      var day = new Date().getDate();

      return {
         events : [
            /*{
               "id":1,
               "start": new Date(year, month, day, 12),
               "end": new Date(year, month, day, 13, 30),
               "title":"Lunch with Mike",
               "CzyWybrany": true
            },
            {
               "id":2,
               "start": new Date(year, month, day, 14),
               "end": new Date(year, month, day, 14, 45),
               "title":"Dev Meeting",
               "CzyWybrany": false
            },
            {
               "id":3,
               "start": new Date(year, month, day + 1, 17),
               "end": new Date(year, month, day + 1, 17, 45),
               "title":"Hair cut",
               "CzyWybrany": true
            },
            {
               "id":4,
               "start": new Date(year, month, day - 1, 8),
               "end": new Date(year, month, day - 1, 9, 30),
               "title":"Team breakfast",
               "CzyWybrany": true
            },
            {
               "id":5,
               "start": new Date(year, month, day + 1, 14),
               "end": new Date(year, month, day + 1, 15),
               "title":"Product showcase",
               "CzyWybrany": false
            },
            {
               "id":6,
               "start": new Date(year, month, day, 10),
               "end": new Date(year, month, day, 11),
               "title":"I'm read-only",
               "CzyWybrany": false
            }*/
         ]
      };
   }
   
   /*
    * Sets up the start and end time fields in the calendar event
    * form for editing based on the calendar event being edited
    */
   function setupStartAndEndTimeFields($startTimeField, $endTimeField, calEvent, timeslotTimes) {

      for (var i = 0; i < timeslotTimes.length; i++) {
         var startTime = timeslotTimes[i].start;
         var endTime = timeslotTimes[i].end;
         var startSelected = "";
         if (startTime.getTime() === calEvent.start.getTime()) {
            startSelected = "selected=\"selected\"";
         }
         var endSelected = "";
         if (endTime.getTime() === calEvent.end.getTime()) {
            endSelected = "selected=\"selected\"";
         }
         $startTimeField.append("<option value=\"" + startTime + "\" " + startSelected + ">" + timeslotTimes[i].startFormatted + "</option>");
         $endTimeField.append("<option value=\"" + endTime + "\" " + endSelected + ">" + timeslotTimes[i].endFormatted + "</option>");

      }
      $endTimeOptions = $endTimeField.find("option");
      $startTimeField.trigger("change");
   }

   var $endTimeField = $("select[name='end']");
   var $endTimeOptions = $endTimeField.find("option");

   //reduces the end time options to be only after the start time options.
   $("select[name='start']").change(function() {
      var startTime = $(this).find(":selected").val();
      var currentEndTime = $endTimeField.find("option:selected").val();
      $endTimeField.html(
            $endTimeOptions.filter(function() {
               return startTime < $(this).val();
            })
            );

      var endTimeSelected = false;
      $endTimeField.find("option").each(function() {
         if ($(this).val() === currentEndTime) {
            $(this).attr("selected", "selected");
            endTimeSelected = true;
            return false;
         }
      });

      if (!endTimeSelected) {
         //automatically select an end date 2 slots away.
         $endTimeField.find("option:eq(1)").attr("selected", "selected");
      }

   });


   var $about = $("#about");

   $("#about_button").click(function() {
      $about.dialog({
         title: "About this calendar demo",
         width: 600,
         close: function() {
            $about.dialog("destroy");
            $about.hide();
         },
         buttons: {
            close : function() {
               $about.dialog("close");
            }
         }
      }).show();
   });

	$("#dev").click(function() {
		klik = !klik;
		$("#calendar").weekCalendar("refresh");
	});
});